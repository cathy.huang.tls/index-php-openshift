<html>
    <head>
        <title>Hello OpenShift!</title>
    </head>

    <style>
     .center h1 h2 {
        text-align: center;
     }
    </style>

    <body>
        <div class="center">
            <h1>My great application !</h1>
            <h2>This app run in <?php echo $_ENV["HOSTNAME"]; ?></h2>
        </div>
    </body>
</html>
